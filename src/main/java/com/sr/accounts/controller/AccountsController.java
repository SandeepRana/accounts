package com.sr.accounts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sr.accounts.AccountsServiceConfig;
import com.sr.accounts.model.Accounts;
import com.sr.accounts.model.Cards;
import com.sr.accounts.model.Customer;
import com.sr.accounts.model.CustomerDetails;
import com.sr.accounts.model.Loans;
import com.sr.accounts.model.Properties;
import com.sr.accounts.repository.AccountsRepository;
import com.sr.accounts.service.client.CardsFeignClient;
import com.sr.accounts.service.client.LoansFeignClient;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
public class AccountsController {
	
	
	@Autowired
	AccountsRepository accountsRepository; 
	
	@Autowired
	AccountsServiceConfig accountsConfig;
	
	@Autowired
	CardsFeignClient cardsClient;
	
	@Autowired
	LoansFeignClient loansClient;
	
	@PostMapping("/myAccount")
	public Accounts getAccountsDetail(@RequestBody Customer customer) {
		Accounts account = accountsRepository.findByCustomerId(customer.getCustomerId());
		return account;
		
	}
	
	@GetMapping("/account/properties")
	public String getPropertyDetails() throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		Properties properties = new Properties(accountsConfig.getMsg(), accountsConfig.getBuildVersion(), accountsConfig.getMailDetails(), accountsConfig.getActiveBranches());
		return ow.writeValueAsString(properties);
	}
	
	@PostMapping("/CustomerDetails")
	@CircuitBreaker(name = "detailsForCustomerSupportApp", fallbackMethod="myCustomerDetailsFallBack")
	public CustomerDetails myCustomerDetails(@RequestBody Customer customer) {
		
		Accounts account = accountsRepository.findByCustomerId(customer.getCustomerId());
		List<Loans> loan = loansClient.getLoansDetails(customer);
		List<Cards> card = cardsClient.getCardDetails(customer);
		
		CustomerDetails custDetails = new CustomerDetails();
		custDetails.setAccounts(account);
		custDetails.setCards(card);
		custDetails.setLoans(loan);
		
		return custDetails;
		
	}
	

	private CustomerDetails myCustomerDetailsFallBack(Customer customer , Throwable t) {
		Accounts account = accountsRepository.findByCustomerId(customer.getCustomerId());
		List<Loans> loan = loansClient.getLoansDetails(customer);
		
		CustomerDetails custDetails = new CustomerDetails();
		custDetails.setAccounts(account);
		custDetails.setLoans(loan);
		
		return custDetails;
		
	}

}
